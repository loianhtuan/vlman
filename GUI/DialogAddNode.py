#Boa:Dialog:Dialog1

import wx
import wx.richtext
from myFunction import *

def create(parent):
    return Dialog1(parent)

[wxID_DIALOG1, wxID_DIALOG1BTCANCEL, wxID_DIALOG1BTOK, 
 wxID_DIALOG1RICHTEXTCTRL1, 
] = [wx.NewId() for _init_ctrls in range(4)]

class Dialog1(wx.Dialog):
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.richTextCtrl1, 1, border=10,
              flag=wx.ALL | wx.EXPAND)
        parent.AddSizer(self.boxSizer2, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_coll_boxSizer2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.btOK, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(14,14), border=0, flag=0)
        parent.AddWindow(self.btCancel, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_boxSizer2_Items(self.boxSizer2)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Dialog.__init__(self, id=wxID_DIALOG1, name='', parent=prnt,
              pos=wx.Point(760, 155), size=wx.Size(334, 353),
              style=wx.DEFAULT_DIALOG_STYLE, title='Dialog1')
        self.SetClientSize(wx.Size(318, 315))

        self.richTextCtrl1 = wx.richtext.RichTextCtrl(id=wxID_DIALOG1RICHTEXTCTRL1,
              parent=self, pos=wx.Point(10, 10), size=wx.Size(298, 272),
              style=wx.WANTS_CHARS | wx.richtext.RE_MULTILINE,
              value=u'Put (or paste) list of node here, line by line, hostname only\nExample:\nnode1.vnlab.net\nnode2.vnlab.net\n(click to remove)')
        self.richTextCtrl1.SetLabel(u'richText')
        self.richTextCtrl1.SetEditable(False)
        self.richTextCtrl1.Bind(wx.EVT_LEFT_DOWN, self.OnRichTextCtrl1SetFocus)

        self.btOK = wx.Button(id=wxID_DIALOG1BTOK, label=u'Add', name=u'btOK',
              parent=self, pos=wx.Point(77, 292), size=wx.Size(75, 23),
              style=0)
        self.btOK.Bind(wx.EVT_BUTTON, self.OnBtOKButton, id=wxID_DIALOG1BTOK)

        self.btCancel = wx.Button(id=wxID_DIALOG1BTCANCEL, label=u'Cancel',
              name=u'btCancel', parent=self, pos=wx.Point(166, 292),
              size=wx.Size(75, 23), style=0)
        self.btCancel.Bind(wx.EVT_BUTTON, self.OnBtCancelButton,
              id=wxID_DIALOG1BTCANCEL)

        self._init_sizers()

    def __init__(self, parent, slice):
        self._init_ctrls(parent)
        self.parent=parent
        self.SetTitle('Add nodes to slice "'+slice+'"')

    def OnRichTextCtrl1SetFocus(self, event):
        if self.richTextCtrl1.GetValue()==u'Put (or paste) list of node here, line by line, hostname only\nExample:\nnode1.vnlab.net\nnode2.vnlab.net\n(click to remove)':
            self.richTextCtrl1.SetValue('')
            self.richTextCtrl1.SetEditable(True)

    def OnBtOKButton(self, event):
        buf=self.richTextCtrl1.GetValue().split('\n')
        nodes=[]
        for a in buf:
            if '[' in a and ']' in a:
                nodes+=[a[a.find('[')+1:a.rfind(']')].strip()]
            else:
                nodes+=[a.strip()]
        wx.PostEvent(self.parent,AddNodeEvent(nodes))
        self.Destroy()
        
    def OnBtCancelButton(self, event):
        self.Destroy()

import time
def main():
    app = wx.PySimpleApp()
    app.frame = Dialog1(None, 'hcmut_loi')
    app.frame.Show()
    app.MainLoop()

if __name__ == "__main__":
    main()
