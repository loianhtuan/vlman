VN-Lab Monitor Beta
written by Tuan Loi Anh - 2013

Requirement:
- Python 2.7
- PyCrypto 2.6
- matplotlib (pip install matplotlib or binary-setup)
- NumPy (pip or binary setup)
- python-dateutil, pyparsing
- fabric 1.8.0 (pip install fabric) (ssh client, not implemented)
  -- paramiko (fabric dependencies)