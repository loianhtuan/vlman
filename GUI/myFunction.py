#!/usr/bin/env python
import threading
import xmlrpclib
import wx
import time
import subprocess
#auth={'AuthMethod':'password','Username':'loianhtuan@gmail.com','AuthString':'planetlab'}
#api=xmlrpclib.ServerProxy('https://192.168.0.121:4443/PLCAPI/')
modules = {}

"""
My Events
"""

myEVT_ACCINFO = wx.NewEventType()
EVT_ACCINFO = wx.PyEventBinder(myEVT_ACCINFO, 1)

myEVT_LOG = wx.NewEventType()
EVT_LOG = wx.PyEventBinder(myEVT_LOG, 1)

myEVT_SLICE_SELECT = wx.NewEventType()
EVT_SLICE_SELECT = wx.PyEventBinder(myEVT_SLICE_SELECT,1)

myEVT_DATA_UPDATE = wx.NewEventType()
EVT_DATA_UPDATE = wx.PyEventBinder(myEVT_DATA_UPDATE,1)

myEVT_ADD_NODE = wx.NewEventType()
EVT_ADD_NODE = wx.PyEventBinder(myEVT_ADD_NODE,1)

class LoginThread(threading.Thread):
    def __init__(self,parent):
        threading.Thread.__init__(self)
        self._parent=parent
    
    def run(self):
        self.Log("Connecting")
        api=xmlrpclib.ServerProxy(self._parent.inUrl.GetValue())
        auth={}
        auth['AuthMethod'] = 'password'
        auth['Username'] = self._parent.inEmail.GetValue()
        auth['AuthString'] = self._parent.inPassword.GetValue()
        try:
            if api.AuthCheck(auth):
                self.Log('Connected')
                self.Log('Getting info')
                person=api.GetPersons(auth)[0]
                info={}
                info['email']=auth['Username']
                info['type']=', '.join(person['roles'])
                info['site']=api.GetSites(auth,person['site_ids'])[0]['name']
                info['slices']=[row['name'] + time.strftime(" (expires %a, %d %b %Y %H:%M:%S)",time.localtime(row['expires'])) for row in api.GetSlices(auth)]
                info['api']=api
                info['auth']=auth
                allnode=[i['hostname'] for i in api.GetNodes(auth,'*',['hostname'])]
                allnode.sort()
                info['allNodes']=allnode
                wx.PostEvent(self._parent,AccInfoEvent(info))
                self._parent.GetParent().GetParent().GetParent().panelLog.SetInfo(info)
                self.Log('Done')
                #self._parent.GetParent().GetParent().GetParent().panel2.SetInfo({'email':'loianhtuan@gmail.com','password':'','type':'pi','site':'hcmut','slices':['12','34']})
        
            else:
                self.Log('Wrong Username:Password')
        except Exception as e:
            self.Log('Exception: '+str(e))
            
    def Log(self,message):
        wx.PostEvent(self._parent,LogEvent(message))
    
    def __del__(self):
        self._parent.btLogin.Enable(True)

class SliceSelectThread(threading.Thread):
    def __init__(self,parent,info):
        threading.Thread.__init__(self)
        self.info=info
        self._parent=parent
    def run(self):
        choice=self._parent.choiceSlice.GetStringSelection()
        slice=choice[:choice.find(' (')]
        self.Log('selected: '+slice)
        api=self.info['api']
        auth=self.info['auth']
        node_ids=api.GetSlices(auth,slice,['node_ids'])[0]['node_ids']
        nodes=[i['hostname'] for i in api.GetNodes(auth,node_ids,['hostname'])]
        wx.PostEvent(self._parent,SliceSelectEvent(nodes))
        
    def Log(self,message):
        wx.PostEvent(self._parent,LogEvent(message))

class LogEvent(wx.PyCommandEvent):
    def __init__(self,message):
        wx.PyCommandEvent.__init__(self,myEVT_LOG,-1)
        self.message=message
    def GetMessage():
        return self.message

class AccInfoEvent(wx.PyCommandEvent):
    def __init__(self,info):
        wx.PyCommandEvent.__init__(self,myEVT_ACCINFO,-1)
        self.info=info
    
    def GetInfo(self):
        return self.info

class SliceSelectEvent(wx.PyCommandEvent):
    def __init__(self,nodes):
        wx.PyCommandEvent.__init__(self,myEVT_SLICE_SELECT,-1)
        self.nodes=nodes
    def GetNodes(self):
        return self.nodes

class DataUpdateEvent(wx.PyCommandEvent):
    def __init__(self,data):
        wx.PyCommandEvent.__init__(self,myEVT_DATA_UPDATE,-1)
        self.data=data
    def GetData(self):
        return self.data
    
class AddNodeEvent(wx.PyCommandEvent):
    def __init__(self,node):
        wx.PyCommandEvent.__init__(self,myEVT_ADD_NODE,-1)
        self.node = node
    def GetNodes(self):
        return self.node
    
def main():
    pass

if __name__ == '__main__':
    main()
