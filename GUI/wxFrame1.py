#Boa:Frame:wxFrame1

import wx
from wx.lib.anchors import LayoutAnchors
import wx.richtext

from wxPanel1 import wxPanel1
from wxPanel2 import wxPanel2
from wxPanel3 import wxPanel3
from wxPanelLog import wxPanelLog
from myFunction import *

def create(parent):
    return wxFrame1(parent)

[wxID_WXFRAME1, wxID_WXFRAME1NOTEBOOK1, wxID_WXFRAME1PANEL1, 
 wxID_WXFRAME1PANEL2, wxID_WXFRAME1PANEL3, wxID_WXFRAME1PANELLOG, 
 wxID_WXFRAME1SPLITTERWINDOW1, 
] = [wx.NewId() for _init_ctrls in range(7)]

class wxFrame1(wx.Frame):
    _custom_classes = {'wx.Panel': ['wxPanel1', 'wxPanel2', 'wxPanel3', 'wxPanelLog']}
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.splitterWindow1, 1, border=0, flag=wx.EXPAND)

    def _init_coll_notebook1_Pages(self, parent):
        # generated method, don't edit

        parent.AddPage(imageId=-1, page=self.panel1, select=True, text=u'Login')
        parent.AddPage(imageId=-1, page=self.panel2, select=False,
              text=u'Account Information')
        parent.AddPage(imageId=-1, page=self.panel3, select=False,
              text=u'Slices')

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_WXFRAME1, name='', parent=prnt,
              pos=wx.Point(423, 94), size=wx.Size(834, 518),
              style=wx.DEFAULT_FRAME_STYLE, title='VN-LAB Monitor')
        self.SetClientSize(wx.Size(818, 480))
        

        self.splitterWindow1 = wx.SplitterWindow(id=wxID_WXFRAME1SPLITTERWINDOW1,
              name='splitterWindow1', parent=self, pos=wx.Point(0, 0),
              size=wx.Size(818, 480), style=0)
        self.splitterWindow1.SetAutoLayout(False)
        self.splitterWindow1.SetToolTipString(u'')
        self.splitterWindow1.SetBorderSize(8)
        self.splitterWindow1.SetThemeEnabled(False)
        self.splitterWindow1.SetLabel(u'')
        self.splitterWindow1.SetHelpText(u'')
        self.splitterWindow1.SetMinimumPaneSize(100)

        self.notebook1 = wx.Notebook(id=wxID_WXFRAME1NOTEBOOK1,
              name='notebook1', parent=self.splitterWindow1, pos=wx.Point(0, 0),
              size=wx.Size(818, 220), style=0)
        self.notebook1.SetMinSize(wx.Size(600, 220))

        self.panel1 = wxPanel1(id=wxID_WXFRAME1PANEL1, name='panel1',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(810, 194),
              style=wx.TAB_TRAVERSAL)

        self.panel2 = wxPanel2(id=wxID_WXFRAME1PANEL2, name='panel2',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(810, 194),
              style=wx.TAB_TRAVERSAL)

        self.panel3 = wxPanel3(id=wxID_WXFRAME1PANEL3, name='panel3',
              parent=self.notebook1, pos=wx.Point(0, 0), size=wx.Size(810, 194),
              style=wx.TAB_TRAVERSAL)

        self.panelLog = wxPanelLog(id=wxID_WXFRAME1PANELLOG, name='panelLog',
              parent=self.splitterWindow1, pos=wx.Point(0, 224),
              size=wx.Size(818, 256), style=wx.TAB_TRAVERSAL)
        self.panelLog.SetMinSize(wx.Size(818, 100))
        self.panelLog.Show(True)
        self.panelLog.SetToolTipString(u'panelLog')
        self.splitterWindow1.SplitHorizontally(self.notebook1, self.panelLog,
              376)

        self._init_coll_notebook1_Pages(self.notebook1)

        self._init_sizers()
        self.Bind(EVT_LOG, self.Log)
        self.Bind(EVT_ACCINFO, self.panel2.SetInfo)
        self.Bind(EVT_SLICE_SELECT, self.panel2.SliceSelect)
        self.Bind(EVT_ADD_NODE, self.panel2.AddNode)

    def Log(self,evt):
        self.panelLog.printLog(evt.message)

    def __init__(self, parent):
        self._init_ctrls(parent)

