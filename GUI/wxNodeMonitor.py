#Boa:Frame:NodeMonitor

import wx, wx.richtext
import time
import threading
import socket
from struct import pack
from collections import deque
from myFunction import *

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import numpy as np
import pylab


def create(parent):
    return NodeMonitor(parent)

[wxID_NODEMONITOR, wxID_NODEMONITORBTVCOTOP, wxID_NODEMONITORCURVAL, 
 wxID_NODEMONITORMETRIC, wxID_NODEMONITORSTATICTEXT1, wxID_NODEMONITORTCUNIT, 
] = [wx.NewId() for _init_ctrls in range(6)]

class NodeMonitor(wx.Frame):
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.staticText1, 0, border=0, flag=0)
        parent.AddSizer(self.boxSizer2, 0, border=0, flag=0)

    def _init_coll_boxSizer2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.metric, 0, border=0, flag=0)
        parent.AddWindow(self.curVal, 0, border=0, flag=0)
        parent.AddWindow(self.tcUnit, 0, border=0, flag=0)
        parent.AddWindow(self.btVcotop, 0, border=0, flag=0)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_boxSizer2_Items(self.boxSizer2)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_NODEMONITOR, name=u'NodeMonitor',
              parent=prnt, pos=wx.Point(552, 162), size=wx.Size(409, 408),
              style=wx.DEFAULT_FRAME_STYLE, title='Frame1')
        self.SetClientSize(wx.Size(393, 370))

        self.staticText1 = wx.StaticText(id=wxID_NODEMONITORSTATICTEXT1,
              label='staticText1', name='staticText1', parent=self,
              pos=wx.Point(0, 0), size=wx.Size(55, 13), style=0)

        self.metric = wx.Choice(choices=['cpu', 'mem', 'rx', 'tx'],
              id=wxID_NODEMONITORMETRIC, name=u'metric', parent=self,
              pos=wx.Point(0, 13), size=wx.Size(64, 21), style=0)
        self.metric.SetSelection(0)
        self.metric.Bind(wx.EVT_CHOICE, self.OnMetric,
              id=wxID_NODEMONITORMETRIC)

        self.curVal = wx.TextCtrl(id=wxID_NODEMONITORCURVAL, name=u'curVal',
              parent=self, pos=wx.Point(64, 13), size=wx.Size(64, 21),
              style=wx.TE_CENTER, value=u'0')
        self.curVal.SetEditable(False)
        self.curVal.Enable(True)

        self.tcUnit = wx.TextCtrl(id=wxID_NODEMONITORTCUNIT, name=u'tcUnit',
              parent=self, pos=wx.Point(128, 13), size=wx.Size(34, 21), style=0,
              value=u'%')

        self.btVcotop = wx.Button(id=wxID_NODEMONITORBTVCOTOP, label=u'Vcotop',
              name=u'btVcotop', parent=self, pos=wx.Point(162, 13),
              size=wx.Size(54, 23), style=0)
        self.btVcotop.Bind(wx.EVT_BUTTON, self.OnBtVcotopButton,
              id=wxID_NODEMONITORBTVCOTOP)

        self._init_sizers()

    def __init__(self, parent, node, slice):
        self._init_ctrls(parent)
        self.node = node
        self.slice = slice
        self.init_canvas()
        self.SetTitle(slice+' @ '+node)
        
    def init_canvas(self):
        self.interval=2 
        self.dpi = 100
        self.fig = Figure((1, 1), dpi=self.dpi)
        self.canvas = FigCanvas(self, -1, self.fig)
        self.axes = self.fig.add_subplot(111)
        self.axes.set_axis_bgcolor('black')
        self.axes.invert_xaxis()
        self.axes.set_xlabel('second')
        
        pylab.setp(self.axes.get_xticklabels(), fontsize=8)
        pylab.setp(self.axes.get_yticklabels(), fontsize=8)

        # plot the data as a line series, and save the reference 
        # to the plotted line series
        #
        self.data = {'cpu':deque(maxlen=60),
                     'mem':deque(maxlen=60),
                     'rx':deque(maxlen=60),
                     'tx':deque(maxlen=60)}
        
        [self.data[i].append(0.1) for i in self.data if i != 'rx' and i != 'tx' ]
        self.plot_data = self.axes.plot(
            self.data['cpu'], 
            linewidth=1,
            color='y',
            )[0]
##        self.plot_data2 = self.axes.plot(
##            self.data['mem'], 
##            linewidth=1,
##            color=(1, 0, 0),
##            )[0]
        self.s = self.init_socket()
        
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.on_redraw_timer, self.redraw_timer)    
        self.Bind(EVT_DATA_UPDATE, self.OnDataUpdate)
        self.redraw_timer.Start(1000*self.interval)
        
        self.boxSizer1.Remove(0)
        self.boxSizer1.PrependWindow(self.canvas, 1, border=0, flag=wx.EXPAND)
        self.update_data(self.s)
        
    
    def on_redraw_timer(self, event):
    # if paused do not add data, but still redraw the plot
    # (to respond to scale modifications, grid change, etc.)
    #
        self.update_data(self.s)
    
    def draw_plot(self):
        """ Redraws the plot
        """
        # when xmin is on auto, it "follows" xmax to produce a 
        # sliding window effect. therefore, xmin is assigned after
        # xmax.
        #

        # for ymin and ymax, find the minimal and maximal values
        # in the data set and add a mininal margin.
        # 
        # note that it's easy to change this scheme to the 
        # minimal/maximal value in the current display, and not
        # the whole data set.
        # 
        metric=self.metric.GetStringSelection()
        
        self.axes.set_xbound(lower=0, upper=60*self.interval)
        ymax=max(self.data[self.metric.GetStringSelection()])
    
        self.axes.set_ybound(lower=-1 if (metric != 'rx' and metric !='tx') else 0, upper=100 if ymax<100 and (metric != 'rx' and metric !='tx') else ymax)
        
        # anecdote: axes.grid assumes b=True if any other flag is
        # given even if b is set to False.
        # so just passing the flag into the first statement won't
        # work.
        #
        self.axes.grid(True, color='gray')
        
        # Using setp here is convenient, because get_xticklabels
        # returns a list over which one needs to explicitly 
        # iterate, and setp already handles this.
        #  
        #pylab.setp(self.axes.get_xticklabels(), 
        #    visible=self.cb_xlab.IsChecked())
        dat=self.data[self.metric.GetStringSelection()]
        self.plot_data.set_xdata(np.arange(0,len(dat)*self.interval,self.interval))
        self.plot_data.set_ydata(np.array(dat))

        #self.plot_data2.set_xdata(np.arange(0,len(self.data)*self.DELAY,self.DELAY)[::-1])
        #self.plot_data2.set_ydata(np.array([i-2 for i in self.data]))
        #self.axes.autoscaley()
        
        self.canvas.draw()
    
    def init_socket(self, port=3120):
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        try:
            s.connect((self.node,port))
            s.send(pack("<I",self.interval))
            return s
        except Exception as e:
            print str(e)
            return None
        
    def update_data(self,s):
        worker = update_thread(self, self.s, self.slice)
        worker.start()
                
    def OnMetric(self, event):
        sel=self.metric.GetStringSelection()
        if sel=='tx' or sel=='rx':
            self.tcUnit.SetValue('KB/s')
        else:
            self.tcUnit.SetValue('%')
    
    def OnDataUpdate(self, event):
        data=event.GetData()
        self.data['cpu'].appendleft(data['cpu'])
        self.data['mem'].appendleft(data['mem'])
        self.data['tx'].appendleft(data['tx'])
        self.data['rx'].appendleft(data['rx'])
        self.curVal.SetValue(str(self.data[self.metric.GetStringSelection()][0]))
        self.draw_plot()

    def OnBtVcotopButton(self, event):
        vcotop=Frame1(self)
        vcotop.Show()
        
class update_thread(threading.Thread):
    def __init__(self,parent,s,slice):
        threading.Thread.__init__(self)
        self.parent = parent
        self.s = s
        self.slice = slice
        
    def run(self):
        s = self.s
        buf=s.recv(4096)
        self.parent.buf=buf
        buf=buf.split('\n')[1:-1]
        for line in buf:
            line = line.split()
            if line[-1] == self.slice:
                self.data={}
                self.data['cpu'] = (float(line[-3]))
                self.data['mem'] = (float(line[-2]))
                self.data['tx'] = (float(line[1]))
                self.data['rx'] = (float(line[3]))
                if isinstance(self.parent, wx.EvtHandler): #check if parent is alive
                    wx.PostEvent(self.parent, DataUpdateEvent(self.data))
                return

[wxID_FRAME1, wxID_FRAME1RICHTEXTCTRL1, 
] = [wx.NewId() for _init_ctrls in range(2)]

class Frame1(wx.Frame):
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.richTextCtrl1, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_FRAME1, name='', parent=prnt,
              pos=wx.Point(666, 257), size=wx.Size(564, 200),
              style=wx.DEFAULT_FRAME_STYLE, title='Frame1')
        self.SetClientSize(wx.Size(564, 200))
        
        self.redraw_timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.OnDataUpdate, self.redraw_timer)
        self.redraw_timer.Start(1000*self.parent.interval)

        self.richTextCtrl1 = wx.richtext.RichTextCtrl(id=wxID_FRAME1RICHTEXTCTRL1,
              parent=self, pos=wx.Point(0, 0), size=wx.Size(564, 179),
              style=wx.richtext.RE_MULTILINE, value=u'')
        self.richTextCtrl1.SetLabel(u'richText')
        self.richTextCtrl1.SetFont(wx.Font(8, wx.SWISS, wx.NORMAL, wx.NORMAL,
              False, u'Lucida Sans Typewriter'))

        self._init_sizers()

    def __init__(self, parent):
        self.parent=parent
        self._init_ctrls(parent)
        self.SetTitle('Vcotop@'+self.parent.node)
        self.richTextCtrl1.SetValue(self.parent.buf)
        
    def OnDataUpdate(self, evt):
        self.richTextCtrl1.SetValue(self.parent.buf)
        
        
def main():
    app = wx.PySimpleApp()
    app.frame = NodeMonitor(None,'node1.vnlab.net','hcmut_bw1')
    app.frame.Show()
    app.MainLoop()

if __name__ == '__main__':
    main()
