#Boa:FramePanel:wxPanel1

import wx, wx.html
import wx.richtext
import xmlrpclib
import subprocess
import time
import threading
import pickle, sys
from myFunction import *

def create(parent):
    return wxPanel1(parent)

[wxID_WXPANEL1, wxID_WXPANEL1BTABOUT, wxID_WXPANEL1BTLOGIN, 
 wxID_WXPANEL1BTLOGOUT, wxID_WXPANEL1INAUTO, wxID_WXPANEL1INEMAIL, 
 wxID_WXPANEL1INPASSWORD, wxID_WXPANEL1INREMEMBER, wxID_WXPANEL1INURL, 
 wxID_WXPANEL1STATICTEXT1, wxID_WXPANEL1STATICTEXT2, wxID_WXPANEL1STATICTEXT3, 
] = [wx.NewId() for _init_ctrls in range(12)]

class wxPanel1(wx.Panel):
    def _init_coll_boxSizerButton_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.btLogin, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.btLogout, 0, border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.btAbout, 0, border=0, flag=0)

    def _init_coll_boxSizerInput_Items(self, parent):
        # generated method, don't edit

        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.inUrl, 0, border=100, flag=wx.LEFT | wx.EXPAND)
        parent.AddWindow(self.inEmail, 0, border=100, flag=wx.EXPAND | wx.LEFT)
        parent.AddWindow(self.inPassword, 0, border=100,
              flag=wx.LEFT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.inRemember, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddWindow(self.inAuto, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSizer(self.boxSizerButton, 0, border=0, flag=wx.ALIGN_CENTER)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizerInput = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizerButton = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_boxSizerInput_Items(self.boxSizerInput)
        self._init_coll_boxSizerButton_Items(self.boxSizerButton)

        self.SetSizer(self.boxSizerInput)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_WXPANEL1, name='', parent=prnt,
              pos=wx.Point(603, 235), size=wx.Size(524, 400),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(508, 362))

        self.inUrl = wx.TextCtrl(id=wxID_WXPANEL1INURL, name=u'inUrl',
              parent=self, pos=wx.Point(100, 8), size=wx.Size(408, 21), style=0,
              value=u'https://vnlab.net/PLCAPI/')

        self.inEmail = wx.TextCtrl(id=wxID_WXPANEL1INEMAIL, name=u'inEmail',
              parent=self, pos=wx.Point(100, 29), size=wx.Size(408, 21),
              style=0, value=u'loianhtuan@gmail.com')

        self.inPassword = wx.TextCtrl(id=wxID_WXPANEL1INPASSWORD,
              name=u'inPassword', parent=self, pos=wx.Point(100, 50),
              size=wx.Size(408, 21), style=wx.TE_PROCESS_ENTER | wx.TE_PASSWORD,
              value='')
        self.inPassword.Bind(wx.EVT_CHAR, self.OnInPasswordChar)

        self.staticText1 = wx.StaticText(id=wxID_WXPANEL1STATICTEXT1,
              label=u'PLCAPI:', name='staticText1', parent=self, pos=wx.Point(8,
              11), size=wx.Size(40, 13), style=0)

        self.staticText2 = wx.StaticText(id=wxID_WXPANEL1STATICTEXT2,
              label=u'Email:', name='staticText2', parent=self, pos=wx.Point(8,
              32), size=wx.Size(29, 13), style=0)

        self.staticText3 = wx.StaticText(id=wxID_WXPANEL1STATICTEXT3,
              label=u'Password:', name='staticText3', parent=self,
              pos=wx.Point(8, 53), size=wx.Size(51, 13), style=0)

        self.inRemember = wx.CheckBox(id=wxID_WXPANEL1INREMEMBER,
              label=u'Remember password', name=u'inRemember', parent=self,
              pos=wx.Point(179, 79), size=wx.Size(150, 14), style=0)
        self.inRemember.SetValue(True)
        self.inRemember.Enable(True)

        self.inAuto = wx.CheckBox(id=wxID_WXPANEL1INAUTO, label=u'Auto login',
              name=u'inAuto', parent=self, pos=wx.Point(179, 93),
              size=wx.Size(150, 14), style=0)
        self.inAuto.SetValue(False)
        self.inAuto.Bind(wx.EVT_CHECKBOX, self.OnInAutoCheckbox,
              id=wxID_WXPANEL1INAUTO)

        self.btLogin = wx.Button(id=wxID_WXPANEL1BTLOGIN, label=u'Login',
              name=u'btLogin', parent=self, pos=wx.Point(131, 115),
              size=wx.Size(75, 23), style=0)
        self.btLogin.Bind(wx.EVT_BUTTON, self.OnBtLoginButton,
              id=wxID_WXPANEL1BTLOGIN)

        self.btLogout = wx.Button(id=wxID_WXPANEL1BTLOGOUT, label=u'Logout',
              name=u'btLogout', parent=self, pos=wx.Point(214, 115),
              size=wx.Size(75, 23), style=0)

        self.btAbout = wx.Button(id=wxID_WXPANEL1BTABOUT, label=u'About',
              name=u'btAbout', parent=self, pos=wx.Point(297, 115),
              size=wx.Size(80, 23), style=0)
        self.btAbout.Bind(wx.EVT_BUTTON, self.OnBtAbout,
              id=wxID_WXPANEL1BTABOUT)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        try:
            f=open("config.cfg","rb")
        except:
            self.conf={'email':self.inEmail.GetValue(),
            'password':self.inPassword.GetValue(),
            'url':self.inUrl.GetValue(),
            'autoLogin':self.inAuto.GetValue()
            }
            return
        try:
            self.conf=pickle.load(f)
            if len(self.conf)>0:
                self.inUrl.SetValue(self.conf['url'])
                self.inAuto.SetValue(self.conf['autoLogin'])
                self.inEmail.SetValue(self.conf['email'])
                self.inPassword.SetValue(self.conf['password'])
                if self.conf['autoLogin']:
                    self.OnBtLoginButton(None)
        except Exception as e:
            wx.PostEvent(self,LogEvent(repr(e)))
            

    def OnBtLoginButton(self, event):
        self.btLogin.Enable(False)
        self.SaveConfig()
        worker = LoginThread(self)
        worker.start()

    def OnInPasswordChar(self, event):
        if event.GetKeyCode()==wx.WXK_RETURN:
            self.OnBtLoginButton(None)
        event.Skip()
    
    def SaveConfig(self):
        try:
            f=open("config.cfg","wb")
            self.conf['email']=self.inEmail.GetValue()
            self.conf['password']=''
            self.conf['url']=self.inUrl.GetValue()
            if self.inRemember.GetValue():
                self.conf['password']=self.inPassword.GetValue()
                wx.PostEvent(self,LogEvent("Password saved to config.cfg"))
            self.conf['autoLogin']=self.inAuto.GetValue()
            pickle.dump(self.conf,f)
            f.close()
        except Exception as e:
            wx.PostEvent(self,LogEvent(str(e)))

    def OnInAutoCheckbox(self, event):
        if not self.inAuto.GetValue():
            try:
                f=open("config.cfg","wb")
                self.conf['autoLogin']=False
                pickle.dump(self.conf,f)
                f.close()
            except Exception as e:
                wx.PostEvent(self,LogEvent(str(e)))

    def OnBtAbout(self, event):
        dlg = AboutBox()
        dlg.ShowModal()
        dlg.Destroy()
    






class HtmlWindow(wx.html.HtmlWindow):
    def __init__(self, parent, id, size=(600,400)):
        wx.html.HtmlWindow.__init__(self,parent, id, size=size)
        if "gtk2" in wx.PlatformInfo:
            self.SetStandardFonts()

    def OnLinkClicked(self, link):
        wx.LaunchDefaultBrowser(link.GetHref())

aboutText = """<p><center>VN-Lab Monitor Beta</center></p>
<p> It is
running on version %(wxpy)s of <b>wxPython</b> and %(python)s of <b>Python</b>.
See <a href="http://wiki.wxpython.org">wxPython Wiki</a></p>"""
class AboutBox(wx.Dialog):
    def __init__(self):
        wx.Dialog.__init__(self, None, -1, "About",
            style=wx.DEFAULT_DIALOG_STYLE|wx.THICK_FRAME|wx.RESIZE_BORDER|
                wx.TAB_TRAVERSAL)
        hwin = HtmlWindow(self, -1, size=(400,200))
        vers = {}
        vers["python"] = sys.version.split()[0]
        vers["wxpy"] = wx.VERSION_STRING
        hwin.SetPage(aboutText % vers)
        btn = hwin.FindWindowById(wx.ID_OK)
        irep = hwin.GetInternalRepresentation()
        hwin.SetSize((irep.GetWidth()+25, irep.GetHeight()+10))
        self.SetClientSize(hwin.GetSize())
        self.CentreOnParent(wx.BOTH)
        self.SetFocus()
            
        
