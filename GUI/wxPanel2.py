#Boa:FramePanel:wxPanel2

import wx
import wx.richtext
import time
from wxNodeMonitor import NodeMonitor
from myFunction import *
from DialogAddNode import Dialog1

def create(parent):
    return wxPanel2(parent)

[wxID_WXPANEL2, wxID_WXPANEL2BTADDNODE, wxID_WXPANEL2BTNODEMONITORS, 
 wxID_WXPANEL2BTRENEW, wxID_WXPANEL2CHOICESLICE, wxID_WXPANEL2INNODELIST, 
 wxID_WXPANEL2OUTEMAIL, wxID_WXPANEL2OUTSITE, wxID_WXPANEL2OUTTYPE, 
 wxID_WXPANEL2STEMAIL, wxID_WXPANEL2STNODES, wxID_WXPANEL2STSITE, 
 wxID_WXPANEL2STSLICES, wxID_WXPANEL2STTYPE, 
] = [wx.NewId() for _init_ctrls in range(14)]

class wxPanel2(wx.Panel):
    def _init_coll_flexGridSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.stEmail, 0, border=0, flag=0)
        parent.AddWindow(self.outEmail, 1, border=10, flag=wx.RIGHT | wx.EXPAND)
        parent.AddWindow(self.stType, 0, border=0, flag=0)
        parent.AddWindow(self.outType, 1, border=0, flag=0)
        parent.AddWindow(self.stSite, 0, border=0, flag=0)
        parent.AddWindow(self.outSite, 0, border=10, flag=wx.RIGHT | wx.EXPAND)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddSpacer(wx.Size(8, 8), border=0, flag=0)
        parent.AddWindow(self.stSlices, 0, border=0, flag=0)
        parent.AddWindow(self.choiceSlice, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.btRenew, 0, border=0, flag=0)
        parent.AddWindow(self.btAddNode, 0, border=0, flag=0)
        parent.AddWindow(self.stNodes, 0, border=0, flag=0)
        parent.AddWindow(self.inNodeList, 1, border=0, flag=wx.EXPAND)
        parent.AddWindow(self.btNodeMonitors, 0, border=0, flag=0)

    def _init_coll_flexGridSizer1_Growables(self, parent):
        # generated method, don't edit

        parent.AddGrowableCol(1)
        parent.AddGrowableCol(3)

    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddSizer(self.flexGridSizer1, 1, border=8,
              flag=wx.ALL | wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.flexGridSizer1 = wx.FlexGridSizer(cols=4, hgap=0, rows=1, vgap=0)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_flexGridSizer1_Items(self.flexGridSizer1)
        self._init_coll_flexGridSizer1_Growables(self.flexGridSizer1)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_WXPANEL2, name='', parent=prnt,
              pos=wx.Point(647, 121), size=wx.Size(487, 442),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(471, 404))

        self.stEmail = wx.StaticText(id=wxID_WXPANEL2STEMAIL, label=u'Email',
              name=u'stEmail', parent=self, pos=wx.Point(8, 8), size=wx.Size(55,
              13), style=0)

        self.outEmail = wx.TextCtrl(id=wxID_WXPANEL2OUTEMAIL, name=u'outEmail',
              parent=self, pos=wx.Point(63, 8), size=wx.Size(234, 21), style=0,
              value=u'')
        self.outEmail.SetEditable(False)
        self.outEmail.Enable(False)

        self.stType = wx.StaticText(id=wxID_WXPANEL2STTYPE, label=u'Type',
              name=u'stType', parent=self, pos=wx.Point(307, 8),
              size=wx.Size(55, 13), style=0)

        self.outType = wx.TextCtrl(id=wxID_WXPANEL2OUTTYPE, name=u'outType',
              parent=self, pos=wx.Point(382, 8), size=wx.Size(100, 21), style=0,
              value=u'')
        self.outType.SetEditable(False)
        self.outType.Enable(False)

        self.stSite = wx.StaticText(id=wxID_WXPANEL2STSITE, label=u'Site',
              name=u'stSite', parent=self, pos=wx.Point(8, 29), size=wx.Size(55,
              13), style=0)

        self.outSite = wx.TextCtrl(id=wxID_WXPANEL2OUTSITE, name=u'outSite',
              parent=self, pos=wx.Point(63, 29), size=wx.Size(234, 21), style=0,
              value=u'')
        self.outSite.SetEditable(False)
        self.outSite.Enable(False)

        self.stSlices = wx.StaticText(id=wxID_WXPANEL2STSLICES,
              label=u'Slices (0)', name=u'stSlices', parent=self,
              pos=wx.Point(8, 50), size=wx.Size(55, 13), style=0)

        self.choiceSlice = wx.Choice(choices=[], id=wxID_WXPANEL2CHOICESLICE,
              name=u'choiceSlice', parent=self, pos=wx.Point(63, 50),
              size=wx.Size(244, 21), style=0)
        self.choiceSlice.SetStringSelection(u'')
        self.choiceSlice.SetLabel(u'')
        self.choiceSlice.SetHelpText(u'')
        self.choiceSlice.SetSelection(1)
        self.choiceSlice.Bind(wx.EVT_CHOICE, self.OnChoiceSliceChoice,
              id=wxID_WXPANEL2CHOICESLICE)

        self.btRenew = wx.Button(id=wxID_WXPANEL2BTRENEW, label=u'Renew',
              name=u'btRenew', parent=self, pos=wx.Point(307, 50),
              size=wx.Size(75, 23), style=0)
        self.btRenew.Enable(False)
        self.btRenew.Bind(wx.EVT_BUTTON, self.OnBtRenewButton,
              id=wxID_WXPANEL2BTRENEW)

        self.stNodes = wx.StaticText(id=wxID_WXPANEL2STNODES, label=u'Nodes',
              name=u'stNodes', parent=self, pos=wx.Point(8, 73),
              size=wx.Size(55, 13), style=0)

        self.inNodeList = wx.CheckListBox(choices=[],
              id=wxID_WXPANEL2INNODELIST, name=u'inNodeList', parent=self,
              pos=wx.Point(63, 73), size=wx.Size(244, 230))
        self.inNodeList.Bind(wx.EVT_LISTBOX, self.OnCheckListBox1Listbox,
              id=wxID_WXPANEL2INNODELIST)

        self.btNodeMonitors = wx.Button(id=wxID_WXPANEL2BTNODEMONITORS,
              label=u'Node Monitors', name=u'btNodeMonitors', parent=self,
              pos=wx.Point(307, 73), size=wx.Size(75, 23), style=0)
        self.btNodeMonitors.Bind(wx.EVT_BUTTON, self.OnBtNodeMonitorsButton,
              id=wxID_WXPANEL2BTNODEMONITORS)

        self.btAddNode = wx.Button(id=wxID_WXPANEL2BTADDNODE,
              label=u'Add nodes', name=u'btAddNode', parent=self,
              pos=wx.Point(382, 50), size=wx.Size(75, 23), style=0)
        self.btAddNode.Enable(False)
        self.btAddNode.Bind(wx.EVT_BUTTON, self.OnBtAddNodeButton,
              id=wxID_WXPANEL2BTADDNODE)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
    
    def SetInfo(self, evt):
        global info
        info=evt.GetInfo()
        self.outEmail.SetValue(info['email'])
        self.outType.SetValue(info['type'])
        self.outSite.SetValue(info['site'])
        self.choiceSlice.SetItems(info['slices'])
        self.stSlices.SetLabel('Slices ({0})'.format(len(info['slices'])))
        self.inNodeList.SetItems(info['allNodes'])

    def OnChoiceSliceChoice(self, event):
        global info
        self.btRenew.Enable(True)
        self.btAddNode.Enable(True)
        choice=self.choiceSlice.GetStringSelection()
        slice=choice[:choice.find(' (')]
        self.Log('Updated: '+slice)
        api=info['api']
        auth=info['auth']
        node_ids=api.GetSlices(auth,slice,['node_ids'])[0]['node_ids']
        checked=[i['hostname'] for i in api.GetNodes(auth,node_ids,['hostname'])]
        unchecked=list(set(info['allNodes'])-set(checked))
        checked.sort()
        unchecked.sort()
        self.inNodeList.SetItems(checked+unchecked)
        self.inNodeList.SetChecked(range(len(checked)))
        
        

    def OnBtRenewButton(self, event):
        global info
        try:
            api, auth = info['api'], info['auth']
            selection = self.GetSlice()
            self.Log('renewing {0} to 1 week'.format(selection))
            slice = api.GetSlices(auth,selection)[0]
            success=api.UpdateSlice(auth,slice['name'],{'expires':slice['expires']+3600*24*7})
            if success == 1:
                self.Log('Renew successfully')
                i=self.choiceSlice.GetSelection()
                self.choiceSlice.SetSelection(-1)
                self.choiceSlice.SetString(i,slice['name'] + time.strftime(" (expires %a, %d %b %Y %H:%M:%S)",time.localtime(slice['expires']+3600*24*7)))
                self.choiceSlice.SetSelection(i)
            else:
                self.Log('Renew failed')
        except Exception as e:
            self.Log(str(e))    
        
    def SliceSelect(self, event):
        pass
    
    def Log(self,message):
        wx.PostEvent(self,LogEvent(message))

    def OnCheckListBox1Listbox(self, event):
        event.Skip()

    def OnBtNodeMonitorsButton(self, event):
        nodelist=self.inNodeList.GetCheckedStrings()
        slice=self.choiceSlice.GetStringSelection()
        slice=slice[:slice.find(' (')]
        for i in nodelist:
            popup=NodeMonitor(self, i, slice)
            popup.Show()

    def OnBtAddNodeButton(self, event):
        dialog = Dialog1(self,self.GetSlice())
        dialog.Show()
    
    def GetSlice(self):
        choice=self.choiceSlice.GetStringSelection()
        selection=choice[:choice.find(' (')]
        return selection
    
    def AddNode(self, event):
        global info
        api=info['api']
        auth=info['auth']
        print "recv",event.GetNodes()
        if api.AddSliceToNodes(auth,self.GetSlice(),event.GetNodes()) == 1:
            self.Log('Add node successfully')
        self.OnChoiceSliceChoice(None)