#Boa:FramePanel:wxPanel3

import wx

def create(parent):
    return wxPanel3(parent)

[wxID_WXPANEL3] = [wx.NewId() for _init_ctrls in range(1)]

class wxPanel3(wx.Panel):
    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.SetSizer(self.boxSizer1)


    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_WXPANEL3, name='', parent=prnt,
              pos=wx.Point(471, 288), size=wx.Size(208, 111), style=self.style)
        self.SetClientSize(wx.Size(192, 73))

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        # style is added as a 'frame attribute' because many styles cannot be
        # changed after creation.
        self.style = wx.TAB_TRAVERSAL
        self.style = style

        self._init_ctrls(parent)

        # This code must be added manually to override the design-time values
        # This is only needed for those FramePanels that are not directly
        # contained (when it's parent sizes it)
        self.SetPosition(pos)
        self.SetSize(size)

    def OnButton1Button(self, event):
        self.staticText1.SetLabel('Click!')
