#Boa:FramePanel:wxPanelLog

import wx
import wx.lib.buttons
import wx.richtext
import time
import sys
import StringIO
import contextlib

@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO.StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

[wxID_WXPANELLOG, wxID_WXPANELLOGBTCLEAR, wxID_WXPANELLOGINPUT, 
 wxID_WXPANELLOGRICHTEXTCTRL1, wxID_WXPANELLOGSTCONSOLE, 
] = [wx.NewId() for _init_ctrls in range(5)]

class wxPanelLog(wx.Panel):
    def _init_coll_boxSizer1_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.btClear, 0, border=8, flag=wx.LEFT)
        parent.AddWindow(self.richTextCtrl1, 1, border=8,
              flag=wx.RIGHT | wx.LEFT | wx.BOTTOM | wx.EXPAND)
        parent.AddSizer(self.boxSizer2, 0, border=8,
              flag=wx.EXPAND | wx.LEFT | wx.RIGHT)

    def _init_coll_boxSizer2_Items(self, parent):
        # generated method, don't edit

        parent.AddWindow(self.stConsole, 0, border=0, flag=wx.ALIGN_CENTER)
        parent.AddWindow(self.inPut, 1, border=0, flag=wx.EXPAND)

    def _init_sizers(self):
        # generated method, don't edit
        self.boxSizer1 = wx.BoxSizer(orient=wx.VERTICAL)

        self.boxSizer2 = wx.BoxSizer(orient=wx.HORIZONTAL)

        self._init_coll_boxSizer1_Items(self.boxSizer1)
        self._init_coll_boxSizer2_Items(self.boxSizer2)

        self.SetSizer(self.boxSizer1)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Panel.__init__(self, id=wxID_WXPANELLOG, name='', parent=prnt,
              pos=wx.Point(499, 276), size=wx.Size(484, 193),
              style=wx.TAB_TRAVERSAL)
        self.SetClientSize(wx.Size(468, 155))

        self.richTextCtrl1 = wx.richtext.RichTextCtrl(id=wxID_WXPANELLOGRICHTEXTCTRL1,
              parent=self, pos=wx.Point(8, 23), size=wx.Size(452, 103),
              style=wx.richtext.RE_READONLY | wx.richtext.RE_MULTILINE,
              value='Log:')
        self.richTextCtrl1.SetEditable(False)
        self.richTextCtrl1.SetFilename(u'')
        self.richTextCtrl1.SetCaretPosition(-1)

        self.btClear = wx.Button(id=wxID_WXPANELLOGBTCLEAR, label=u'Clear log',
              name=u'btClear', parent=self, pos=wx.Point(8, 0), size=wx.Size(75,
              23), style=0)
        self.btClear.Bind(wx.EVT_BUTTON, self.OnBtClearButton,
              id=wxID_WXPANELLOGBTCLEAR)

        self.stConsole = wx.StaticText(id=wxID_WXPANELLOGSTCONSOLE,
              label=u'Console:', name=u'stConsole', parent=self, pos=wx.Point(8,
              138), size=wx.Size(55, 13), style=0)

        self.inPut = wx.TextCtrl(id=wxID_WXPANELLOGINPUT, name=u'inPut',
              parent=self, pos=wx.Point(63, 134), size=wx.Size(397, 21),
              style=wx.TE_PROCESS_ENTER, value=u'')
        self.inPut.Enable(False)
        self.inPut.Bind(wx.EVT_CHAR, self.OnInPutChar)

        self._init_sizers()

    def __init__(self, parent, id, pos, size, style, name):
        self._init_ctrls(parent)
        
    def printLog(self, message):
        t=time.strftime("%d-%m-%Y %H:%M:%S: ", time.localtime())
        self.richTextCtrl1.AppendText('\n'+t+message)
        self.richTextCtrl1.ScrollIntoView(self.richTextCtrl1.GetCaretPosition(),wx.WXK_PAGEDOWN)
        
    def clearLog(self):
        self.richTextCtrl1.SetValue('Log:')

    def OnBtClearButton(self, event):
        self.richTextCtrl1.SetValue('Log:')
        event.Skip()

    def OnInPutChar(self, event):
        if event.GetKeyCode()==wx.WXK_RETURN:
            api = self.info['api']
            auth = self.info['auth']
            help = self.ConsoleHelp
            code = self.inPut.GetValue()
            self.inPut.SetValue('')
            with stdoutIO() as s:
                exec code
            self.printLog(code)
            self.printLog('output: '+s.getvalue())
        event.Skip()
    
    def SetInfo(self,info):
        self.info=info
        self.inPut.Enable(True)
        
    def ConsoleHelp(self):
        self.printLog('Help: api and auth is predefine variable for PLCAPI communicate\nexample: print api.GetNodes(auth)')
