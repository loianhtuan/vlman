#!/usr/bin/python2

################################################################
# Reimplement of CoTop @ CoDeeN project
# Written by Loi Anh Tuan (loianhtuan@gmail.com)
# Requirement:
# - util-vserver 0.30.215
# - procps 3.2.8 (know-as 'top')
################################################################

import subprocess
import sys
import getopt
import time
import threading
import SocketServer
from struct import unpack
from decimal import *
from collections import deque

class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
	def handle(self):
		# data = self.request.recv(1024)
		self.request.settimeout(2)
		delay=0
		try:
			buf=self.request.recv(4)
			if len(buf)<4:
				delay=int(buf)
		except:
			self.request.sendall(vcotop())
			return
		if 'GET' in buf or 'POST' in buf:
			self.request.sendall(vcotop())
			return
		while True:
			if delay == 0:
				delay=unpack('<L',buf)[0]
			self.request.sendall(vcotop())
			if delay < 100:
				time.sleep(delay)
			else:
				return

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
	pass

def usage():
	print 'Vcotop 0.1 by Loi Anh Tuan (loianhtuan@gmail.com)\n'
	print 'Usage: ./vcotop.py [-dhvV] [-p port] [-i interval\n'
	print 'command:'
	print '\t-h\t\tThis help text'
	print '\t-p port\t\tListening port (default: 3120)'
	print '\t-v\t\tVerbose'

def debug(text):
	if verbose:
		try:
			print "DEBUG: "+text if isinstance(text,str) else "DEBUG: "+str(text)
		except:
			print "EXCEPTION: debug can't print"

def calc_cpu():
	# call "vps aux", return a dictionary indicate cpu portion used by context id
	# need to check in multi-processor node
	vps = subprocess.Popen(["vps","aux"], stdout=subprocess.PIPE)
	awk = subprocess.Popen(["awk",'$3 != "0" && $3 != "1"'], stdin=vps.stdout, stdout=subprocess.PIPE)
	(out, err) = awk.communicate()
	lines = out.split('\n')[1:] #ignore summary portion
	result={}
	for line in lines[:-1]: #exclude last 2 '\n'
		line = line.split()
		# line = ['USER', 'PID', 'CONTEXT_ID', 'CONTEXT', '%CPU', '%MEM', 'VSZ', 'RSS', 'TTY', 'STAT', 'START', 'TIME', 'COMMAND']
		user = line[3]
		cpu = Decimal(line[4])
		result[user] = result[user]+cpu if user in result else cpu
	return result

def netmon():
	while True:
		for c in active:
			if c not in contexts:
				contexts[c]=deque(maxlen=DEQUE_SIZE)
				for i in xrange(DEQUE_SIZE):
					contexts[c].append((-1,-1))
			try:
				with open('/proc/virtual/'+c+'/cacct') as cacct:
					cacct=cacct.read().split('\n')[3].split()[1:3]
					rx=int(cacct[0][cacct[0].find('/')+1:]) #bytes
					tx=int(cacct[1][cacct[1].find('/')+1:]) #bytes
					last_rx,last_tx = contexts[c][0]
					r_rx,r_tx = last_rx&0xffffffff,last_tx&0xffffffff
					if rx<r_rx:
						rx+=4294967296
					if tx<r_tx:
						tx+=4294967296
					contexts[c].appendleft((last_rx+(rx-r_rx),last_tx+(tx-r_tx)))
			except:
				active.remove(c)
		time.sleep(NETWORK_PERIOD)

def vcotop():
	# The top portion of the screen shows the standard measurements reported by top.
	proc = subprocess.Popen(["vtop", "-bn1i"], stdout=subprocess.PIPE)
	(out, err) = proc.communicate()
	head='\n'.join(out.split('\n')[:6])
	TOTAL_MEM = Decimal(head[head.find('Mem:')+4:head.find('k total')].strip())/Decimal(1024) # total mem in MB

	body = '{0:>3} {1:>7} {2:>7} {3:>7} {4:>7} {5:>3} {6:>6} {7:>6} {8:>4} {9:>4} {10}\n'\
	.format('CTX' ,'TX', 'TX15','RX', 'RX15','#PR', 'PMEM','VMEM','%CPU','%MEM','NAME')
	# The bottom portion shows slices and their properties. 
	# These fields are taken from vserser-stat, and are as follows:
	# CTX - the slice's context ID, sort of like a user number
	# TX1 - transmit bandwidth in Kb/s over the past minute
	# TX15 - transmit bandwidth in Kb/s over the past 15 minutes
	# RX1 - receive bandwidth in Kb/s over the past minute
	# RX15 - receive bandwidth in Kb/s over the past 15 minutes
	# #PR - the number of processes owned by this slice
	# PMEMMB - the amount of physical memory (in MB) used by this slice
	# VMEMMB - the amount of virtual memory (in MB) used by this slice
	# %CPU - what fraction of the CPU is being consumed by this slice
	# %MEM - what fraction of memory is being consumed by this slice
	# NAME - the slice's name
	VPS = calc_cpu()
	proc = subprocess.Popen(["vserver-stat"], stdout=subprocess.PIPE)
	(out, err) = proc.communicate()
	out=out[60:].split('\n') #split into a list of line
	for line in out[:-1]: # exclude the last '\n'
		line = line.split()
		# line = ['CTX', 'PROC', 'VSZ', 'RSS', 'userTIME', 'sysTIME', 'UPTIME', 'NAME']
		# print line
		CTX = line[0]
		PR = line[1]
		active.add(CTX)
		PMEM = line[3]
		VMEM = line[2]
		MEM_P = (Decimal(line[3][:-1] if line[3] is not '0' else 0)\
				*Decimal({'K':'0.001','M':'1','G':'1000','0':'1'}[line[3][-1]])\
				/TOTAL_MEM*Decimal(100)).quantize(Decimal('1.0')) #calc memory usage fraction
		NAME = line[7]
		CPU_P = VPS[NAME].quantize(Decimal('0.1')) if NAME in VPS else 0.0
		if CTX in contexts:
			now_rx, now_tx = contexts[CTX][0]
			oma_rx, oma_tx = contexts[CTX][1] #oma = one PERIOD ago
			fma_rx, fma_tx = contexts[CTX][DEQUE_SIZE-1] #fma = fifteen minutes ago
			# if (CTX=='512'):
			# 	debug('now_rx '+str(now_rx))
			# 	debug('now-oma '+str(now_rx-oma_rx))
			if oma_rx < 0: oma_rx,oma_tx = now_rx, now_tx #init value
			TX = ((now_tx - oma_tx) / Decimal(NETWORK_PERIOD*1024)).quantize(Decimal('1.0'))
			RX = ((now_rx - oma_rx) / Decimal(NETWORK_PERIOD*1024)).quantize(Decimal('1.0'))
			if fma_rx >= 0:  #init value
				TX15 = ((now_tx - fma_tx) / Decimal(15*60*1024)).quantize(Decimal('1.0'))
				RX15 = ((now_rx - fma_rx) / Decimal(15*60*1024)).quantize(Decimal('1.0'))
			else:
				TX15 = 'NA'
				RX15 = 'NA'
			
		else:
			TX = 'NA'
			RX = 'NA'
			TX15 = 'NA'
			RX15 = 'NA'
		body += '{0:>3} {1:>7} {2:>7} {3:>7} {4:>7} {5:>3} {6:>6} {7:>6} {8:>4} {9:>4} {10}\n'\
		.format( CTX ,  TX,    TX15,  RX,    RX15,  PR,    PMEM,  VMEM,  CPU_P, MEM_P, NAME)
	# time.sleep(3)
	# while True:
	# 	print contexts['512'][0],contexts['512'][60/NETWORK_PERIOD],contexts['512'][DEQUE_SIZE-1]
	# 	time.sleep(1)
	return head+body

def vcotop_daemon(port):
	server = ThreadedTCPServer(('0', port), ThreadedTCPRequestHandler)
	server.serve_forever()

def main(argv):
	####### global variable ###############
	global verbose, NETWORK_PERIOD, DEQUE_SIZE, contexts, active
	NETWORK_PERIOD = 2 #seconds
	DEQUE_SIZE = 15*60/NETWORK_PERIOD
	contexts={} #need to improve
	active=set()
	verbose=False
	#######################################

	port=3120
	try:
		opts, args = getopt.getopt(argv[1:],"hvp:",["port="])
	except getopt.GetoptError:
		usage()
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			usage()
			sys.exit()
		elif opt == '-v':
			verbose = True
		elif opt in ("-p", "--port"):
			port = int(arg)
	t1=threading.Thread(target=netmon)
	t1.daemon=True
	t1.start()
	vcotop_daemon(port)
	sys.exit()

if __name__ == "__main__":
	main(sys.argv)

'''
known bugs:
-		Local "top" without 'k'. TOTAL_MEM = Decimal(head[head.find('Mem:')+4:head.find('k total')].strip())/Decimal(1024) # total mem in MB
decimal.InvalidOperation: Invalid literal for Decimal: '8058024 total,  6030124 used,  2027900 free,   955104 buffers\nKiB Swap:  7757820 total,   179336 used,  7578484 free,  2721948 cached'

-		global active variable needs to be updated even there is no request, and also being removed to change network state to NA
'''